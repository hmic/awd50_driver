#include "AWD50.h"
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <termios.h>
#include <cstdint>
#include <cmath>
#include <math.h>
#define _USE_MATH_DEFINES
#include <sys/ioctl.h>

namespace awd50{
    //=========================================
    // Serial port
    //=========================================
    SerialPort::SerialPort(std::string device, int verbose){        
        inited = false;
        this->verbose = verbose;
        file_descriptor = open(device.c_str(), O_RDWR | O_NOCTTY);
        if( file_descriptor == -1 ){
            if( this->verbose > 0)
                printf("[SerialPort] error: can't open device %s\n", device.c_str());
            return;
        }
        // Flush away any bytes previously read or written.
        int result = tcflush(file_descriptor, TCIOFLUSH);
        if (result){
            if( this->verbose > 0)
                printf("[SerialPort] warning: tcflush failed\n");  // just a warning, not a fatal error
        }

        struct termios options;
        result = tcgetattr(file_descriptor, &options);
        if (result){
            if( this->verbose > 0)
                printf("[SerialPort] error: tcgetattr failed\n");
            close(file_descriptor);
            return;
        }
        
        // Turn off any options that might interfere with our ability to send and receive raw binary bytes.
        options.c_iflag &= ~(INLCR | IGNCR | ICRNL | IXON | IXOFF);
        options.c_oflag &= ~(ONLCR | OCRNL);
        options.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
            
        // disable parity
        options.c_lflag &= ~PARENB;
        // set stop bit
        options.c_lflag |= CSTOPB;
        // set bit number
        options.c_cflag |= CS8;
        // disable flow control
        options.c_cflag &= ~CRTSCTS; 
        
        options.c_cc[VTIME] = 1;
        options.c_cc[VMIN] = 0;
        
        cfsetospeed(&options, B9600);
        cfsetispeed(&options, cfgetospeed(&options));
        
        result = tcsetattr(file_descriptor, TCSANOW, &options);
        if (result){
            if( this->verbose > 0)
                printf("[SerialPort] error: tcsetattr failed\n");
            close(file_descriptor);
            return;
        }  
                
        printf("[SerialPort] port opened at descriptor %i\n", file_descriptor);  
        inited = true;
    }
    
    int SerialPort::write_port(uint8_t * buffer, size_t len){    
        if( len == 0)        
            size_t len = sizeof(buffer);        
        ssize_t result = write(file_descriptor, buffer, len);
        if (result != (ssize_t)len){
            if( this->verbose > 0)
                printf("[SerialPort:write_port] error: failed to write to port\n");
            return -1;
        }    
        return 0;
    }

    ssize_t SerialPort::read_port(uint8_t * buffer, size_t len){
        if( len == 0)
            len = sizeof(buffer);
        size_t received = 0;
        while (received < len){
            ssize_t r = read(file_descriptor, buffer + received, len - received);
            if (r < 0){
                if( this->verbose > 0)
                    printf("[SerialPort:read_port] error: failed to read \n");
                return -1;
            }
            if (r == 0){
                // Timeout
                break;
            }
            received += r;
        }
        return received;
    }
    
    void SerialPort::flush_port(){
        usleep(1000);
        ioctl(file_descriptor, TCOFLUSH, 2); 
    }
    
    //=========================================
    // Modbus hacked commands
    //=========================================

    ModbusHackedCommand::ModbusHackedCommand(uint8_t* command, size_t command_len, uint8_t* response, size_t response_len){
        this->command = command;
        this->command_len = command_len;
        this->response = response;
        this->response_len = response_len;
    }

    uint16_t ModbusHackedCommand::calc_crc16_v1_modbus( const uint8_t *command, uint8_t& low, uint8_t &high, size_t len ) const{
        uint16_t crc = 0xFFFF;
        unsigned int i = 0;
        uint8_t bit = 0;
        if( len == 0 )
            len = sizeof(command);        
        for( i = 0; i < len; i++ ){
            crc ^= command[i];
            for( bit = 0; bit < 8; bit++ ){
                if( crc & 0x0001 ){
                    crc >>= 1;
                    crc ^= 0xA001;
                    
                }
                else{
                    crc >>= 1;    
                }
            }
        }
        low = crc & 0xFF;
        high = crc >> 8;
        return crc;
    }
    
    void ModbusHackedCommand::set_command_crc(){
        uint8_t crc_low, crc_high;
        calc_crc16_v1_modbus(this->command, crc_low, crc_high, this->command_len-2);
        this->command[this->command_len-1] = crc_high;
        this->command[this->command_len-2] = crc_low;
    }
    
    bool ModbusHackedCommand::is_response_ok_crc(uint8_t* resv_res){
        uint8_t crc_low, crc_high;
        calc_crc16_v1_modbus(resv_res, crc_low, crc_high, response_len-2);
        //printf("crc ok");
        if( (crc_low == response[response_len-2] and crc_high == response[response_len-1]) ){
            for(size_t i = 0 ; i < response_len -2; i++){
                //printf("%i %i\n",i, response_len);
                if( response[i] != resv_res[i] )
                    return false;
            }
            return true;            
        }                
        return false;
    }
    
    AWD50RequestReg::AWD50RequestReg(uint8_t addr, uint8_t reg){
        // BAD
        command_len = 13;        
        uint8_t cmd[] = {addr, 0x03, 0x00, reg, 0x00, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};        
        command = new uint8_t(13);
        for(size_t i = 0; i < command_len; i++)
            command[i] = cmd[i];
        
        response_len = 7;
        response = new uint8_t(response_len);
        uint8_t res[] = {0x09, 0x03, 0x2, 0x00, 0x00, 0x00, 0x00};
        for(size_t i = 0; i < response_len; i++)
            response[i] = res[i];
        set_command_crc();
    }
    
    int16_t AWD50RequestReg::get_reg() const{
        uint8_t hi = response[3];
        uint8_t lo = response[4];
        int16_t reg = hi << 8 | lo;
        return reg;
    }
    
    AWD50SetReg::AWD50SetReg(uint8_t addr, uint8_t reg, int16_t value){
        command_len = 11;
        uint8_t value_high = (value >> 8 ) & 0xFF;
        uint8_t value_low = value & 0xFF;
        uint8_t cmd[] = {addr, 0x10, 0x00, reg, 0x00, 0x01, 0x02, value_high, value_low, 0x00, 0x00};        
        command = new uint8_t(command_len);
        for(size_t i = 0; i < command_len; i++)
            command[i] = cmd[i];
        set_command_crc();
        
        response_len = 8;
        response = new uint8_t(response_len);
        uint8_t res[] = {0x09, 0x10, 0x00, reg, 0x00, 0x01, 0x00, 0x00};
        for(size_t i = 0; i < response_len; i++)
            response[i] = res[i];
    }   

    //=========================================
    // AWD 50 driver interface
    //=========================================

    AWD50::AWD50(SerialPort *serial_port, int TPR, uint8_t slave_addr, int verbose){
        this->TPR = TPR;
        this->verbose = verbose;
        this->slave_addr = slave_addr;
        inited = false;        
        this->serial_port = serial_port;                     
                
        if( get_register(AWD_REG::Vz, this->Vz) ){   
            if( this->verbose > 0)
                printf("[AWD50] init Vz is %i\n", this->Vz);
        }
        else{
            if( this->verbose > 0)
                printf("[AWD50] error: failed to get Vz\n");
            return;
        }
        
        if( get_register(AWD_REG::ET, this->ET) ){            
            if( this->verbose > 0)
                printf("[AWD50] init ET is %i\n", this->ET);
        }
        else{
            if( this->verbose > 0)
                printf("[AWD50] error: failed to get ET\n");
            return;
        }
        
        if( get_register(AWD_REG::Kv0, this->Kv0) ){            
            if( this->verbose > 0)
                printf("[AWD50] init Kv0 is %i\n", this->Kv0);
        }
        else{
            if( this->verbose > 0)
                printf("[AWD50] warn: failed to get Kv0\n");
        }
        
        if( get_register(AWD_REG::Kv, this->Kv) ){            
            if( this->verbose > 0)
                printf("[AWD50] init Kv is %i\n", this->Kv);
        }
        else{
            if( this->verbose > 0)
                printf("[AWD50] warn: failed to get Kv\n");
        }
        
        if( get_register(AWD_REG::Kiv, this->Kiv) ){            
            if( this->verbose > 0)
                printf("[AWD50] init Kiv is %i\n", this->Kiv);
        }
        else{
            if( this->verbose > 0)
                printf("[AWD50] warn: failed to get Kiv\n");
        }
        
//         if( !nullify_encoders() ) {
//             printf("[AWD50] warn: failed to nullicate encoders\n");
//         }          
//         else{
//             printf("[AWD50] encoders successfully nullificated\n");
//         }
        
        encoder_dt = pow(2, ET) * (Tadc * 0.001);
        if( this->verbose > 0)
            printf("[AWD50] encoder dt is %f seconds\n", encoder_dt);
        
        inited = true;    
    }

    bool AWD50::send_req_with_answer(ModbusHackedCommand* command, size_t retries){
        size_t retry_cnt = 0;        
        do{           
//             /print_command(command->command, command->command_len);
            serial_port->write_port(command->command, command->command_len);            
            uint8_t* buffer = new uint8_t[command->response_len];            
            serial_port->read_port(buffer, command->response_len);            
            command->response = buffer;
            //print_command(buffer, command->response_len);            
            if( command->is_response_ok_crc(buffer) )
                return true;            
            retry_cnt++;
            if( this->verbose > 0)
                printf("[AWD50:send_req_with_answer] warning: response crc is wrong\n");
        }while(retry_cnt < retries);        
        return false;
    }
    
    bool AWD50::get_register(uint8_t reg, int16_t& value){
        AWD50RequestReg reg_req(slave_addr, reg);
        bool res = send_req_with_answer(&reg_req);
        if( res ){
            value = reg_req.get_reg();
            return true;
        }
        return false;
    }
    
    bool AWD50::set_register(uint8_t reg, int16_t value){
        AWD50SetReg speed_cmd(slave_addr, reg, value);
        return send_req_with_answer(&speed_cmd);
    }
    
    bool AWD50::set_speed_rad_s(double speed, int& speed_raw){
        //int16_t speed_raw;
        //speed_raw = (2*M_PI * (speed/encoder_dt))/TPR - this->Vz;
        speed_raw = (speed * encoder_dt * TPR)/(2*M_PI) - Vz;
        if( this->verbose > 0)
            printf("[AWD50:set_speed_rad_s] rad/s: %f RSV: %i\n", speed, speed_raw);
        return set_speed_raw(speed_raw);
    }

    void AWD50::print_command(uint8_t* command, size_t len){
        if( len == 0)
            len = sizeof(command);
        printf("cmd [");
        for(size_t i = 0 ; i < len; i++){
            printf("%#02x ", command[i], command[i]);
        }
        printf("]\n");
    }
    
    bool AWD50::get_speed_rad_s(double &speed){
        int16_t V;
        bool res = get_register(AWD_REG::V, V);
        if(!res)
            return false;        
        speed = (2* M_PI * (V + this->Vz)) / (encoder_dt * TPR);
        return true;        
    }        
    
    bool AWD50::set_Kv(int16_t Kv_new){
        AWD50SetReg setKv_cmd = AWD50SetReg(slave_addr, AWD_REG::Kv, Kv_new);        
        if( send_req_with_answer(&setKv_cmd) ){
            this->Kv = Kv_new;
            return true;
        }
        return false;        
    }
    
    bool AWD50::set_Kv0(int16_t Kv0_new){
        AWD50SetReg setKv0_cmd = AWD50SetReg(slave_addr, AWD_REG::Kv0, Kv0_new);        
        if( send_req_with_answer(&setKv0_cmd) ){
            this->Kv0 = Kv0_new;
            return true;
        }
        return false;        
    }
    
    bool AWD50::set_Kiv(int16_t Kiv_new){
        AWD50SetReg setKiv_cmd = AWD50SetReg(slave_addr, AWD_REG::Kiv, Kiv_new);        
        if( send_req_with_answer(&setKiv_cmd) ){
            this->Kiv = Kiv_new;
            return true;
        }
        return false;        
    }
    
};
