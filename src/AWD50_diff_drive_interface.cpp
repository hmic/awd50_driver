#include "AWD50_diff_drive_interface.h" 
#include <controller_manager/controller_manager.h>
#include <ros/callback_queue.h>
#include <dynamic_reconfigure/server.h>
#include "awd50_driver/AWD50PIConfig.h"
#include <mutex>

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

namespace awd50{

    //AWD50DiffDriveInterface::AWD50DiffDriveInterface(){}

    AWD50DiffDriveInterface::AWD50DiffDriveInterface(std::string device, uint8_t left_addr, uint8_t right_addr, std::string left_wheel_joint_name, std::string right_wheel_joint_name, int TPR, int gear_rate, int verbose){
        this->gear_rate = gear_rate;
        inited = false;
        // AWD50     
        SerialPort* sp = new SerialPort(device, verbose -1);    
        if( !sp->is_inited() ){
            ROS_ERROR("[AWD50 diff drive] fail to open port %s",device.c_str());
            exit(-1);    
        }
        left_awd = new AWD50(sp, TPR, left_addr, verbose);
        if( !left_awd->is_inited() ){
            ROS_ERROR("[AWD50 diff drive] fail to start ADW50 device at addr %#02x",left_addr);
            exit(-2);    
        }
        right_awd = new AWD50(sp, TPR, right_addr, verbose);
        if( !right_awd->is_inited() ){            
            ROS_ERROR("[AWD50 diff drive] fail to open port %s",device.c_str());
            ROS_ERROR("[AWD50 diff drive] fail to start ADW50 device at addr %#02x",right_addr);
            exit(-2);
        }
        
        // DIFF DRIVE INTERFACE
        pos[0] = 0.0; pos[1] = 0.0;
        vel[0] = 0.0; vel[1] = 0.0;
        eff[0] = 0.0; eff[1] = 0.0;
        cmd[0] = 0.0; cmd[1] = 0.0;        
        
        hardware_interface::JointStateHandle left_wheel_state_handle( left_wheel_joint_name, pos, vel, eff);
        joint_state_interface.registerHandle(left_wheel_state_handle);
        
        hardware_interface::JointStateHandle right_wheel_state_handle( right_wheel_joint_name, pos+1, vel+1, eff+1);
        joint_state_interface.registerHandle(right_wheel_state_handle);
        
        registerInterface(&joint_state_interface);
        
        hardware_interface::JointHandle left_wheel_handle(joint_state_interface.getHandle(left_wheel_joint_name), cmd);
        joint_vel_interface.registerHandle(left_wheel_handle);
        
        hardware_interface::JointHandle right_wheel_handle(joint_state_interface.getHandle(right_wheel_joint_name), cmd+1);
        joint_vel_interface.registerHandle(right_wheel_handle);
        
        registerInterface(&joint_vel_interface);        
    }

    void AWD50DiffDriveInterface::read(){    
        int raw_speed;
        if( !(left_awd->set_speed_rad_s(cmd[0] * gear_rate, raw_speed) && right_awd->set_speed_rad_s(cmd[1] * gear_rate, raw_speed))){
            ROS_WARN("[AWD50 diff drive] write connection error!");
            //exit(-3);
        }
    }

    void AWD50DiffDriveInterface::write(double dt){
        double sl, sr;
        if( !(left_awd->get_speed_rad_s(sl) && right_awd->get_speed_rad_s(sr))){
            ROS_WARN("[AWD50 diff drive] read connection error!");
            //exit(-3);
            return;
        }
        
        vel[0] = sl / gear_rate;
        vel[1] = sr / gear_rate;
        
        pos[0] += vel[0] * dt;
        pos[1] += vel[1] * dt;        
    }
    
    bool AWD50DiffDriveInterface::set_pi(int16_t lKv0, int16_t lKv, int16_t lKiv, int16_t rKv0, int16_t rKv, int16_t rKiv){
        bool result = true;
        result &= left_awd->set_Kv0(lKv0);
        result &= left_awd->set_Kv(lKv);
        result &= left_awd->set_Kiv(lKiv);
        
        result &= right_awd->set_Kv0(rKv0);
        result &= right_awd->set_Kv(rKv);
        result &= right_awd->set_Kiv(rKiv);
        return result;
    }
}

awd50::AWD50DiffDriveInterface *awd50_dd;
std::mutex reconf_mtx;

void config_callback(awd50_driver::AWD50PIConfig &config, uint32_t level){
    ROS_INFO("[AWD50 diff drive] reconfigure request ([Left] Kv0 Kv Kiv): %i %i %i ([Right] Kv0 Kv Kiv): %i %i %i",
             config.left_Kv0, config.left_Kv, config.left_Kiv,
             config.right_Kv0, config.right_Kv, config.right_Kiv);

    reconf_mtx.lock();
    bool result = awd50_dd->set_pi(config.left_Kv0, config.left_Kv, config.left_Kiv, config.right_Kv0, config.right_Kv, config.right_Kiv);
    reconf_mtx.unlock();
    if(!result)
        ROS_WARN("[AWD50 diff drive] fail to update PI coefficients");
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "awd50_diff_drive_interface_node");
    ros::NodeHandle nh_;
    ros::NodeHandle nh("~");        
    
    std::string lw_joint = "lwheel1_j";
    std::string rw_joint = "rwheel1_j";
    double rate_hz = 100;
    std::string device = "/dev/ttyUSB0";
    int left_addr = 0x07;
    int right_addr = 0x09;
    int TPR = 190;
    int verbose = 2;
    int gear_rate = 1;
    
    nh.getParam("lw_joint", lw_joint);
    nh.getParam("rw_joint", rw_joint);
    nh.getParam("rate_hz", rate_hz);
    nh.getParam("port", device);
    nh.getParam("left_addr", left_addr);
    nh.getParam("right_addr", right_addr);
    nh.getParam("TPR", TPR);
    nh.getParam("verbose", verbose);
    nh.getParam("gear_rate", gear_rate);
                
    awd50_dd = new awd50::AWD50DiffDriveInterface(device, left_addr, right_addr, lw_joint, rw_joint, TPR, gear_rate, verbose);
    controller_manager::ControllerManager cm(awd50_dd, nh_);    
    
    dynamic_reconfigure::Server<awd50_driver::AWD50PIConfig> server;
    dynamic_reconfigure::Server<awd50_driver::AWD50PIConfig>::CallbackType f;
    
    f = boost::bind(&config_callback, _1, _2);
    server.setCallback(f);
                
    ros::AsyncSpinner spinner(1);
    spinner.start();
    
    ros::Rate rate(rate_hz);
    ros::Time prev_time = ros::Time::now();
    while(ros::ok()){        
        ros::Duration dt = ros::Time::now() - prev_time;
        prev_time = ros::Time::now();
        reconf_mtx.lock();
        awd50_dd->write(dt.toSec());            
        cm.update(ros::Time::now(), dt);
        awd50_dd->read();
        reconf_mtx.unlock();
        rate.sleep();                
    }
    spinner.stop();
    awd50_dd->stop();
    return 0;
}
