/*
 * Author: Moscowsky Anton
 * dual driver test
 */
#include "AWD50.h"
#include <signal.h>
#define _USE_MATH_DEFINES
using namespace awd50;

AWD50 * awd50_1;
AWD50 * awd50_2;

void stop_awd50(int s){      
    printf("Stopping...\n");
    bool res1 = false, res2 = false;
    int cntr = 0;
    do{
        if( !res1 )
            res1 = awd50_1->stop();
        if( !res2 )
            res2 = awd50_2->stop();
        printf(res1 && res2 ? "OK!\n" : "FAIL! retry... ");        
    }while(!(res1 && res2) && cntr < 20);
    exit(1); 
}

int main()
{         
    SerialPort sp("/dev/ttyUSB0");
    if( !sp.is_inited() )
        return -4;
    
    AWD50 awd50d1(&sp, 190, 0x07, 2);
    AWD50 awd50d2(&sp, 190, 0x09, 2);
    awd50_1 = &awd50d1;
    awd50_2 = &awd50d2;
    
    if(!awd50d1.is_inited()){
        printf("Fail init device 1!");
        return -1;
    }
    if(!awd50d2.is_inited()){
        printf("Fail init device 2!");
        return -1;
    }
    
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = stop_awd50;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);            
        
    double speed = -12;
    printf("Setting speed for 1 %f...", speed);
    int speed_raw;          
    bool res = awd50d1.set_speed_rad_s(speed, speed_raw);
    printf(res ? "OK!\n" : "FAIL!\n");
    if (!res)
        return -2;
    printf("Setting speed for 2 %f...", speed);
    res = awd50d2.set_speed_rad_s(speed, speed_raw);
    printf(res ? "OK!\n" : "FAIL!\n");
    if (!res)
        return -2;            
    
    double speed_rad_per_s_1,  speed_rad_per_s_2;
    while(true){
        awd50d1.ssleep(0.1);                                  
        awd50d1.get_speed_rad_s(speed_rad_per_s_1);
        awd50d2.get_speed_rad_s(speed_rad_per_s_2);
                
        printf("Target speed (%f %f)[rad/s], calculated speed (%f %f)[rad/s]\n", speed, speed, speed_rad_per_s_1, speed_rad_per_s_2);        
    }
    
    return 0;    
}
