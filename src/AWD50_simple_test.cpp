/*
 * Author: Moscowsky Anton
 * Performs motor rotation from one direction to another, reading raw encoder data
 * 
 */
#include "AWD50.h"
#include <unistd.h>

using namespace awd50;

int main()
{
    SerialPort sp("/dev/ttyUSB0");
    AWD50 awd50(&sp, 1, 0x07, 2);
    
    if(!awd50.is_inited()){
        printf("Fail init device!");
        return -1;
    }
    int speed = -1000;
    int step = 100;
    bool res;
    int16_t encoders;
    printf("Requesting encoders...", speed);        
    res = awd50.get_speed_raw(encoders);
    printf(res ? "OK: %i\n" : "FAIL!\n", encoders);        
    while( speed <= 1000 ){        
        printf("Setting speed %i...", speed);
        res = awd50.set_speed_raw(speed);
        printf(res ? "OK!\n" : "FAIL!\n");
        awd50.ssleep(1);
        speed+=step;
        printf("Requesting encoders...", speed);
        res = awd50.get_speed_raw(encoders);
        printf(res ? "OK: %i\n" : "FAIL!\n", encoders);
    }
    printf("Stopping...");
    res = awd50.set_speed_raw(0);
    printf(res ? "OK!\n" : "FAIL!\n");
    for(size_t i = 0 ; i < 5; i++){
        awd50.ssleep(0.5);
        printf("Requesting encoders...", speed);        
        res = awd50.get_speed_raw(encoders);
        printf(res ? "OK: %i\n" : "FAIL!\n", encoders);        
    }
    return 0;    
}
