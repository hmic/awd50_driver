/*
 * Author: Moscowsky Anton
 * 
 */
#include "AWD50.h"
#include <signal.h>
#define _USE_MATH_DEFINES
using namespace awd50;

AWD50 * awd50l;

void stop_awd50(int s){          
    printf("Stopping...\n");
    bool res;
    int cntr = 0;
    do{
        res = awd50l->stop();
        printf(res ? "OK!\n" : "FAIL! retry... ");        
    }while(!res and cntr++ < 10);
    exit(1); 
}

int main()
{         
    SerialPort sp("/dev/ttyUSB0");
    if( !sp.is_inited() )
        return -4;
    
    int TPR = 1000;
    
    AWD50 awd50d(&sp, TPR, 0x07, 2);
    awd50l = &awd50d;
    
    if(!awd50d.is_inited()){
        printf("Fail init device!");
        return -1;
    }
    
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = stop_awd50;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);            
    
    int new_Kv0 = 32000;
    int new_Kv = 200;
    int new_Kiv = 1000;
    
    if( awd50d.set_Kv(new_Kv) ){
        printf("Kv changed to %i\n",new_Kv);
    }
    else{
        printf("Failed to set Kv\n");
    }
    
    if( awd50d.set_Kv0(new_Kv0) ){
        printf("Kv0 changed to %i\n",new_Kv0);
    }
    else{
        printf("Failed to set Kv0\n");
    }
    
    if( awd50d.set_Kiv(new_Kiv) ){
        printf("Kiv changed to %i\n",new_Kiv);
    }
    else{
        printf("Failed to set Kiv\n");
    }
        
    double speed = 100;
    printf("Setting speed %f...", speed);
    int speed_raw;
    bool res = awd50d.set_speed_rad_s(speed, speed_raw);
    printf(res ? "OK!\n" : "FAIL!\n");
    //printf("Raw speed: %i\n", speed_raw);
    if (!res)
        return -2;
    
    int16_t prev_raw_encoders;
    int16_t V;
    double speed_rad_per_s;    
    while(true){
        awd50d.ssleep(0.025);        
        int16_t raw_encoders;
        res = awd50d.get_speed_raw(raw_encoders);
        if (!res)
            return -3;                
        int16_t enc = (raw_encoders - prev_raw_encoders);
        
        awd50d.get_register(AWD_REG::V, V);
        awd50d.get_speed_rad_s(speed_rad_per_s);
        
        printf("Speed in: %f [r/s] %i [rsv]; V: %i; speed out: %f [r/s]; d_enc: %i; raw_ec: %i\n", speed, speed_raw, V, speed_rad_per_s, enc, raw_encoders);
        prev_raw_encoders = raw_encoders;
    }
    
    return 0;    
}
