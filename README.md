![version](https://img.shields.io/badge/version-0.0.1-blue) ![ROS](https://img.shields.io/badge/ROS-Noetic-blue)

# awd50_driver
C++ wrapper and ROS driver for [AWD50](https://www.ellab.su/catalog/bukd/bukdpt/awd50-24.html) motor driver.  
 
## 1. Install
```shell
cd <your ROS workspace>/src
git clone https://gitlab.com/hmic/awd50_driver
catkin build awd50_driver # or catkin_make
```
## 2. Driver setup
To control wheel motor AWD50 needs to be setup. It can be done with software provided on [official page](https://www.ellab.su/catalog/bukd/bukdpt/awd50-24.html).  
Example of settings are provided in .ini [file](doc/coeffs.ini) but it can't be loaded to AWD50 as if, because it has some specific info like serial number. So in the following section main parameters are provided and explained:  
 * In Control (Управление) tab:
    * __Vz__ (=0) shift for readed speed
    * __Vsz__ (=0) shift for set speed
 * In Coefficients (Коэффициенты) tab:
    * _Addr_ (leave default or any you like) modbus address of device, can be change if two drivers is needed.
    * __ModW__ (=1 - Speed (Скорость)) control speed directly
    * __ModVs__ (=0 - RSV) set target value by RS485 interface
    * _ModIs_ (=0 - RSM) but seems it doesn't matter
    * __ModV__ (=0 2 - ENC) feedback from quadric encoders
    * __ModFinRev__ (=None (Нет)) disable reverse end sensor
    * __ModFinForw__ (=None (Нет)) disable forward end sensor
    * __FinRev__ (=1) enable reverse rotation
    * __FinForw__ (=1) enable forward rotation
    * __KeyEn__ (=1) enable rotaion
    * __PIV_SET1__ (=EEPROM) read proportional parameter from EEPROM
    * __PII_SET2__ (=EEPROM) read integral parameter from EEPROM
    * _Kv_ (depends of your system) proportional coefficient, can be set throught wrapper
    * _Kv0_ (depends of your system) multiplier for input speed, can be set throught wrapper
    * _Kiv_ (depends of your system) integral coeffitient, can be set throught wrapper
    * _Vdead_ (=0, but at your choice) range for speed that ignored, `if V < Vdead then V = 0`

![main tab](doc/main.png)
![coeff tab](doc/coeff.png)  

When set, parameters have to been wrote into AWD and then in its EEPROM.


## 3. Nodes

### 3.1. awd50_simple_test
Actually ROS-free demo, providing simple motor rotation with raw encoder data output.
```shell
rosrun awd50_driver awd50_simple_test
```

### 3.2. awd50_encoder_test
One more ROS-free demo, providing motor rotation and encoder reading in rad/s values.
```shell
rosrun awd50_driver awd50_encoder_test
```

### 3.3. awd50_dual_test
ROS-free demo with two devices connected via same RS485-interface.
```shell
rosrun awd50_driver awd50_dual_test
```

### 3.4. awd50_diff_drive_interface_node
ROS-node for two AWD50 drivers, which provides interface for diff_drive ROS-controller.
#### 3.3.1. Parameters
 - ~lw_joint (string, lwheel1_j) joint of left wheel
 - ~rw_joint (string, rwheel1_j) joint of right wheel
 - ~rate_hz (double, 100 [hz]) write-read rate
 - ~port (string, /dev/ttyUSB0) device port name
 - ~left_addr (int, 0x07) modbus addres of left AWD50 driver
 - ~right_addr (int, 0x09) modbus addres of right AWD50 driver
 - ~TPR (int, 190) ticks per round of motor encoder
#### Reconf parameters
 - ~left_Kv0 (int, 2000) Left target speed multiplier coeffitient
 - ~left_Kv (int, 32000) Left proportional coeffitient
 - ~leftKiv (int, 10000) Left integral coeffitient
 - ~right_Kv0 (int, 2000) Right target speed multiplier coeffitient
 - ~right_Kv (int, 32000) Right proportional coeffitient
 - ~rightKiv (int, 10000) Right integral coeffitient

#### 3.3.2. Subscribed topics
 - __/<base_controller_name>/cmd_vel__ (geomrty_msgs/Twist) target robot velocity
#### 3.3.3. Published topics
 - __/<base_controller_name>/odom__ (nav_msgs/Odometry) robot odometry
 - __/joint_states__ (sensors_msgs/JointState) states of wheel joints

Simple example how to run it on simple robot provided in `launch/dummy_diff_drive_test.launch`, start it with:
```shell
roslaunch awd50_driver dummy_diff_drive_test.launch
```
Send command to move and it will display the result
```shell
rostopic pub /mobile_base_controller/cmd_vel geometry_msgs/Twist "linear:
  x: 1.0
  y: 0.0
  z: 0.0
angular:
  x: 0.0
  y: 0.0
  z: 0.0" -r 10
```

![rviz](doc/rviz.png)

## 4. Connect
AWD50's manual (available on offcial cite, and provided in this repo) has complte information, but below is shorty explained how to use it with RS485 and quadric encoder. This connections required settings provided above.
### 4.1. One driver
![one driver connect](doc/one_driver_connect.png)
 - GND linkage between ADW50 and RS485 is optional, but recommended.
### 4.2. Two drivers
![two drivers connect](doc/two_drivers_connect.png)
 - GND linkage between ADW50s and RS485 is optional, but recommended.
 - On second driver jumper JP1 must be closed
