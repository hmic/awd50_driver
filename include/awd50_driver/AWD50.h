/* ADW50 interface
 * 
 */
#ifndef AWD50_H_
#define AWD50_H_
#include <unistd.h>
#include <string>

namespace awd50{        
    
    //=========================================
    // Serial port
    //=========================================
    class SerialPort{
    public:
        SerialPort(std::string device, int verbose = 1);
        
        int write_port(uint8_t * buffer, size_t len = 0);
        ssize_t read_port(uint8_t * buffer, size_t len = 0);
        void flush_port();
        
        inline bool is_inited(){return inited;};
        
    private:
        bool inited;
        int file_descriptor, verbose;
    };
    
    //=========================================
    // Modbus RTU commands and AWD50 registers
    //=========================================    
    enum AWD_REG{
        Vz = 0x1B,
        ET = 0x16,
        Kv0 = 0xE,
        Kv = 0xF,
        Kiv = 0x10,
        V = 0x66,
        RSV = 0x74,
        EN = 0x7D,
    };
    
    class ModbusHackedCommand{
    public:
        ModbusHackedCommand(){}
        ModbusHackedCommand(uint8_t* command, size_t command_len, uint8_t* response, size_t response_len);
        
        uint8_t* command;
        size_t command_len;
        uint8_t* response;
        size_t response_len;
                
        bool is_response_ok_crc(uint8_t* resv_res);
        
    protected:
        uint16_t calc_crc16_v1_modbus( const uint8_t *command, uint8_t& low, uint8_t &, size_t len = 0) const;
        
        void set_command_crc();
    };    
    
    class AWD50RequestReg : public ModbusHackedCommand{
    public:
        AWD50RequestReg(uint8_t addr, uint8_t reg);        
        int16_t get_reg() const;
        
    private:        
    };    
    
    class AWD50SetReg : public ModbusHackedCommand{
    public:
        AWD50SetReg(uint8_t addr, uint8_t reg, int16_t value);
    private:        
    };    
    
    //=========================================
    // AWD50 driver interface wrapper
    //=========================================

    /*
     * device - serial port name, eg. /dev/ttyUSB0
     * TPR - ticks per round of encoder, leave any if only raw data is needed
     * slave_addr - modbus RTU device addres
     */
    class AWD50{
    public:
        AWD50(SerialPort* serial_port, int TPR = 1, uint8_t slave_addr = 0x00, int verbose = 1);
        
        // high lvl
        inline bool is_inited(){return inited;};
        bool get_speed_rad_s(double& speed);
        bool set_speed_rad_s(double speed, int& speed_raw);
        inline bool stop(){
            int nothing;
            return set_speed_rad_s(0, nothing);
        }
        
        bool set_Kv0(int16_t Kv0_new);
        bool set_Kv(int16_t Kv_new);
        bool set_Kiv(int16_t Kiv_new);
        
        // mid lvl        
        bool get_register(uint8_t reg, int16_t& value);
        bool set_register(uint8_t reg, int16_t value);
        inline bool set_speed_raw(int16_t speed){return set_register(AWD_REG::RSV, speed);}
        inline bool get_speed_raw(int16_t &encoder_ticks){return get_register(AWD_REG::EN, encoder_ticks);}
        //inline bool nullify_encoders(){return set_register(AWD_REG::EN, 0) && set_register(AWD_REG::EN+1, 0);}
                
        // utils
        inline void ssleep(float seconds){usleep(seconds*1000000);}        
        
    private:        
        bool inited;
        SerialPort* serial_port;
        
        bool send_cmd_with_answer(const ModbusHackedCommand* command, size_t retries = 0);
        bool send_req_with_answer(ModbusHackedCommand* command, size_t retries = 0);
                
        void print_command(uint8_t* command, size_t len = 0);
        
        int TPR, verbose;
        
        const float Tadc = 0.5;
        int16_t Vz, ET, Kv0, Kv, Kiv;
        double encoder_dt;
        uint8_t slave_addr;
                    
    };
};

#endif //AWD50_H_
