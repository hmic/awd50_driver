/* ADW50 diff drive interface
 * 
 */
#ifndef AWD50_DIFF_DRIVE_H_
#define AWD50_DIFF_DRIVE_H_
#include "AWD50.h"
#include <ros/ros.h>
#include <hardware_interface/robot_hw.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <std_msgs/Float32MultiArray.h>

namespace awd50 {
    
    class AWD50DiffDriveInterface: public hardware_interface::RobotHW{
    public:
        AWD50DiffDriveInterface(std::string device, uint8_t left_addr, uint8_t right_addr, std::string left_wheel_joint_name, std::string right_wheel_joint_name, int TPR, int gear_rate, int verbose);
        
        void read();
        void write(double dt);
        
        bool set_pi(int16_t lKv0, int16_t lKv, int16_t lKiv, int16_t rKv0, int16_t rKv, int16_t rKiv);
        
        inline bool stop(){return left_awd->stop() && right_awd->stop();}
            
        
    private:
        AWD50 *left_awd, *right_awd;
        bool inited;
        int gear_rate;
        
        hardware_interface::JointStateInterface joint_state_interface;
        hardware_interface::VelocityJointInterface joint_vel_interface;
        
        double cmd[2]; // rad/s
        double pos[2]; // rad
        double vel[2]; // rad/s
        double eff[2]; //              
    };
}

#endif //AWD50_DIFF_DRIVE_H_
